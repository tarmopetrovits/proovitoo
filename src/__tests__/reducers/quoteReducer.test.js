import quoteReducer from '../../reducers/quoteReducer';
import quotelist from '../fixtures/quotelist';

test("should set default state", () => {
	const state = quoteReducer(undefined, {type: '@@INIT'});
	expect(state).toEqual('');
})

test("should delete quote by ID", () => {
	const action = {
		type: 'DELETE_QUOTE',
		ID: quotelist[1].ID
	}

	const state = quoteReducer(quotelist, action);
	expect(state).toEqual([quotelist[0], quotelist[2]]);
})

test("should note delete quote if no ID found", () => {
	const action = {
		type: 'DELETE_QUOTE',
		ID: 'none'
	}

	const state = quoteReducer(quotelist, action);
	expect(state).toEqual(quotelist);
})

test("should add quote", () => {
	const quote = {
		ID: '098asd',
		content: '098 content',
		title: '098 title',
		link: '098 link',
	}
	const action = {
		type: 'ADD_QUOTE',
		quote
	}

	const state = quoteReducer(quotelist, action);
	expect(state).toEqual([...quotelist, quote]);
})

test("should edit quote by ID", () => {
	const content = 'new content';
	const action = {
		type: 'EDIT_QUOTE',
		ID: quotelist[0].ID,
		updates: {
			content
		}
	}

	const state = quoteReducer(quotelist, action);
	expect(state[0].content).toBe(content);
})


test("should not edit quote if no ID", () => {
	const content = 'new content';
	const action = {
		type: 'EDIT_QUOTE',
		ID: 'none',
		updates: {
			content
		}
	}

	const state = quoteReducer(quotelist, action);
	expect(state).toEqual(quotelist);
})