import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import toJSON from 'enzyme-to-json';
import Header from '../../components/Header';


configure({adapter: new Adapter()});
test('Should render header', ()=> {
	const wrapper = shallow(<Header/>);
	expect(toJSON(wrapper)).toMatchSnapshot();
})