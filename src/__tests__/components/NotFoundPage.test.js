import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import toJSON from 'enzyme-to-json';
import NotFoundPage from '../../components/NotFoundPage';


configure({adapter: new Adapter()});
test('Should render not found page', ()=> {
	const wrapper = shallow(<NotFoundPage/>);
	expect(toJSON(wrapper)).toMatchSnapshot();
})