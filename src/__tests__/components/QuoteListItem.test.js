import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import toJSON from 'enzyme-to-json';
import QuoteListItem from '../../components/QuoteListItem';
import quotes from '../fixtures/quotelist';

configure({adapter: new Adapter()});
test("Should render quote list item", () => {
	const wrapper = shallow(<QuoteListItem { ...quotes[0] } />);
	expect(toJSON(wrapper)).toMatchSnapshot();
});

