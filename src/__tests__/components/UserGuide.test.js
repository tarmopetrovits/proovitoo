import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import toJSON from 'enzyme-to-json';
import UserGuide from '../../components/UserGuide';


configure({adapter: new Adapter()});
test('Should render user guide page', ()=> {
	const wrapper = shallow(<UserGuide/>);
	expect(toJSON(wrapper)).toMatchSnapshot();
})