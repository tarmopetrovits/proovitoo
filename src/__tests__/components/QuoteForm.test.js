import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import toJSON from 'enzyme-to-json';
import QuoteForm from '../../components/QuoteForm';
import quotes from '../fixtures/quotelist';


configure({adapter: new Adapter()});
test('Should render quote form', ()=> {
	const wrapper = shallow(<QuoteForm/>);
	expect(toJSON(wrapper)).toMatchSnapshot();
});

test('Should render quote form with data', ()=> {
	const wrapper = shallow(<QuoteForm quote={quotes[1]}/>);
	expect(toJSON(wrapper)).toMatchSnapshot();
});

test("Should render error for invalid form submission", () => {
	const wrapper = shallow(<QuoteForm/>);
	expect(toJSON(wrapper)).toMatchSnapshot();
	wrapper.find('form').simulate('submit', {
		preventDefault: ()=>{}
	});
	expect(wrapper.state('error')).toBe(true);
	expect(toJSON(wrapper)).toMatchSnapshot();
});

test("Should set title on input change", () => {
	const value = 'New Title';
	const wrapper = shallow(<QuoteForm/>);
	wrapper.find('input').at(0).simulate('change', {
		target: {value}
	});
	expect(wrapper.state('title')).toBe(value);
});

test("Should set link on input change", () => {
	const value = 'New Link';
	const wrapper = shallow(<QuoteForm/>);
	wrapper.find('input').at(1).simulate('change', {
		target: {value}
	});
	expect(wrapper.state('link')).toBe(value);
});

test("Should set content on textarea change", () => {
	const value = 'New Content';
	const wrapper = shallow(<QuoteForm/>);
	wrapper.find('textarea').simulate('change', {
		target: {value}
	});
	expect(wrapper.state('content')).toBe(value);
});

test("Should call onSubmit prop for valid form submission", () => {
	const onSubmitSpy = jest.fn();
	const wrapper = shallow(<QuoteForm quote={quotes[0]} onSubmit={onSubmitSpy}/>);
	wrapper.find('form').simulate('submit', {
		preventDefault: ()=>{}
	});
	expect(wrapper.state('error')).toBe(undefined);
	expect(onSubmitSpy).toHaveBeenLastCalledWith({
		content: quotes[0].content,
		link: quotes[0].link,
		title: quotes[0].title
	});
});