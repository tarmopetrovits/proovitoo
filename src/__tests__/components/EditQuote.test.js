import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import toJSON from 'enzyme-to-json';
import { EditQuote } from '../../components/EditQuote';
import quotes from '../fixtures/quotelist';

configure({adapter: new Adapter()});
test('Should render EditQuote page', ()=> {
	const editQuote = jest.fn();
	const history = { push: jest.fn() };
	const wrapper = shallow(<EditQuote editQuote={editQuote} quote={quotes[0]} history={history}/>);
	expect(toJSON(wrapper)).toMatchSnapshot();
});

test('Should handle editQuote', ()=> {
	const editQuote = jest.fn();
	const history = { push: jest.fn() };
	const wrapper = shallow(<EditQuote editQuote={editQuote} quote={quotes[0]} history={history}/>);
	wrapper.find('QuoteForm').prop('onSubmit')(quotes[0]);
	expect(history.push).toHaveBeenLastCalledWith('/');
	expect(editQuote).toHaveBeenLastCalledWith(quotes[0].ID, quotes[0]);
});
