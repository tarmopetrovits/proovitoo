import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import toJSON from 'enzyme-to-json';
import { QuoteList } from '../../components/QuoteList';
import quotes from '../fixtures/quotelist';

configure({adapter: new Adapter()});
test("Should render quoteList with quotes", () => {
	const wrapper = shallow(<QuoteList quotes={quotes} />);
	expect(toJSON(wrapper)).toMatchSnapshot();
});

test("Should render quoteList with empty message", () => {
	const wrapper = shallow(<QuoteList quotes={[]} />);
	expect(toJSON(wrapper)).toMatchSnapshot();
});
