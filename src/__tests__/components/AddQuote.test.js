import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import toJSON from 'enzyme-to-json';
import AddQuote from '../../components/AddQuote';

configure({adapter: new Adapter()});
test('Should render AddQuote page', () => {
	const addQuote = jest.fn();
	const wrapper = shallow(<AddQuote addQuote={addQuote}/>);
	expect(toJSON(wrapper)).toMatchSnapshot();
});
