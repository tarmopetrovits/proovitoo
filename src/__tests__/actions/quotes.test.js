import {addQuote, editQuote, deleteQuote} from "../../actions/quotes";

test("should setup remove quote action object", () => {
	const action = deleteQuote({ID: '123asd'});
	expect(action).toEqual({
		type: 'DELETE_QUOTE',
		ID: '123asd'
	})
})

test("should setup edit quote action object", () => {
	const action = editQuote('123asd', {content:"test content", link:"test link", title:"test title"});
	expect(action).toEqual({
		type: 'EDIT_QUOTE',
		ID: '123asd',
		updates: {
			content:"test content",
			link:"test link",
			title:"test title"
		}
	})
})

test("should setup add quote action object", () => {
	var data = [{ID: '1234', title:'test title', content:'test content', link:'test link'}];
	const action = addQuote(data);
	expect(action).toEqual({
		type: 'ADD_QUOTE',
		quote: {ID: '1234', title:'test title', content:'test content', link:'test link'}
	})
})
