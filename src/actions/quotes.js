import axios from 'axios';

const PATH_BASE = 'http://quotesondesign.com';
const PATH_POSTS = '/wp-json/posts?';
const POST_PARAM = 'filter[orderby]=rand&filter[posts_per_page]=1';

const url = `${PATH_BASE}${PATH_POSTS}${POST_PARAM}`;

export const fetchQuoteDetails = () => {
	return function(dispatch){
		return axios.get(url).then(({ data }) => {
			dispatch(addQuote(data));
		});
	}
}

export const addQuote = (data) => ({
	type: 'ADD_QUOTE',
	quote: data[0]
});

export const deleteQuote = ({ ID } = {}) => ({
	type: 'DELETE_QUOTE',
	ID
});

export const editQuote = (ID, updates) => ({
	type: 'EDIT_QUOTE',
	ID,
	updates
});