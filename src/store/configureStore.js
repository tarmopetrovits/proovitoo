import { createStore, applyMiddleware } from 'redux';
import thunk from "redux-thunk";
import quoteReducer from '../reducers/quoteReducer';

export default () => {
	const store = createStore(
		quoteReducer, applyMiddleware(thunk)
	);
	return store;
}
