import React, { Component } from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import Header from './components/Header';
import QuoteList from './components/QuoteList';
import UserGuide from './components/UserGuide';
import NotFoundPage from './components/NotFoundPage';
import EditQuote from './components/EditQuote';

const store = configureStore();

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<BrowserRouter>
					<div>
						<Header/>
						<Switch>
							<Route path="/" component={QuoteList} exact={true} />
							<Route path="/userguide" component={UserGuide} />
							<Route path="/edit/:id" component={EditQuote} />
							<Route component={NotFoundPage}/>
						</Switch>
					</div>
				</BrowserRouter>
			</Provider>
		);
	}
}

export default App