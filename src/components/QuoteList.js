import React from 'react';
import { connect } from 'react-redux';
import QuoteListItem from './QuoteListItem';
import AddQoute from './AddQuote';

export const QuoteList = (props) => (
	<div className="content-container">
		{props.quotes.length ? (
			props.quotes.map((quote)=>{
				return (
					<QuoteListItem key={quote.ID} {...quote}/>
				)
			})
		): (
			<div className="message-warning">Sorry the list is empty, please press the "Add new quote"</div>
		)}
		<AddQoute/>
	</div>
);

const mapStateToProps = (state) => {
	return {
		quotes: state
	}
}

export default connect(mapStateToProps)(QuoteList);