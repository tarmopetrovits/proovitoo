import React from 'react';

const UserGuide = () => (
	<div className="userguide-container">
		<article>
			<ul>
				<li>Start with create new react app -> https://github.com/facebook/create-react-app</li>
				<li>Next find a way how to fetch data with Axios</li>
				<li>Start building React app</li>
				<li>Install node-sass</li>
				<li>Writing some tests using Jest and Enzyme</li>
			</ul>
			<ul>
				<li>Start app: npm run start</li>
				<li>Run tests: npm run test</li>
			</ul>
			<ul>
				<li>Resource list:</li>
				<li>https://blog.logrocket.com/data-fetching-in-redux-apps-a-100-correct-approach-4d26e21750fc</li>
				<li>https://www.udemy.com/react-2nd-edition/</li>
			</ul>
			<ul>
				<li>Difficulties, in process:</li>
				<li>AddQuote test</li>
				<li>QuoteListItem test</li>
				<li>Still getting duplicates when App is not in inspect-mode</li>
			</ul>
		</article>
	</div>
);

export default UserGuide;