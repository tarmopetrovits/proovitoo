import React from 'react';
import { Link } from 'react-router-dom';

const NotFoundPage = () => (
	<div className="message-warning">
		It seems you are lost - 404 ! <Link to="/">Go home</Link>
	</div>
);

export default NotFoundPage;