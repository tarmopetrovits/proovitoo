import React from 'react';
import { connect } from 'react-redux';
import { fetchQuoteDetails } from '../actions/quotes';

export class AddQuote extends React.Component{
	onClick = () => {
		this.props.getPosts();
	};
	render(){
		return(
			<div className="add-quote-container">
				<button
					onClick={this.onClick}
				>Add new quote
				</button>
			</div>
		)
	}
}

const mapDispatchToProps = { getPosts: fetchQuoteDetails }
export default connect(undefined,mapDispatchToProps)(AddQuote);