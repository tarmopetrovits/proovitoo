import React from 'react';

export default class QuoteForm extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			content: props.quote ? props.quote.content: '',
			link: props.quote ? props.quote.link : '',
			title: props.quote ? props.quote.title : '',
			error: undefined
		}
	}

	onContentChange = (ev) => {
		const content = ev.target.value;
		this.setState(() => ({ content }));
	}

	onLinkChange = (ev) => {
		const link = ev.target.value;
		this.setState(() => ({ link }));
	}

	onTitleChange = (ev) => {
		const title = ev.target.value;
		this.setState(() => ({ title }));
	}

	onSubmit = (ev) => {
		ev.preventDefault();
		if (!this.state.content || !this.state.link || !this.state.title) {
			this.setState(() => ({
				error: true
			}))
		} else {
			this.setState(() => ({ error: undefined }))
			this.props.onSubmit({
				content: this.state.content,
				title: this.state.title,
				link: this.state.link,
			})
		}
	}

	render() {
		return (
			<div>
				{this.state.error && <div className="message-warning">Please enter content, title and link</div>}
				<form onSubmit={this.onSubmit}>
					<input
						type="text"
						placeholder="Title"
						autoFocus
						value={this.state.title}
						onChange={this.onTitleChange}
					/>
					<input
						type="text"
						placeholder="Link"
						value={this.state.link}
						onChange={this.onLinkChange}
					/>
					<textarea
						placeholder="Content"
						value={this.state.content}
						onChange={this.onContentChange}
					></textarea>
					<button>Update Quote</button>
				</form>
			</div>
		)
	}
}