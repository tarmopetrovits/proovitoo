import React from 'react';
import { connect } from 'react-redux';
import { editQuote } from '../actions/quotes';
import QuoteForm from './QuoteForm';

export class EditQuote extends React.Component{
	onSubmit = (quote) => {
		this.props.editQuote(this.props.quote.ID, quote)
		this.props.history.push('/')
	}
	render(){
		return(
			<div>
				<QuoteForm
					quote={this.props.quote}
					onSubmit={this.onSubmit}
				/>
			</div>
		)
	}
}

const mapStateToProps = (state, props) => {
	return {
		quote: state.find((quote) => {
			return quote.ID === parseInt(props.match.params.id)
		})
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		editQuote: (id, quote) => dispatch(editQuote(id, quote))
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(EditQuote);