import React from 'react';
import {NavLink} from 'react-router-dom';

const Header = () => (
	<header>
		<NavLink to="/" activeClassName="is-active" exact>Home</NavLink>
		<NavLink to="/userguide" activeClassName="is-active">Userguide</NavLink>
	</header>
);

export default Header;