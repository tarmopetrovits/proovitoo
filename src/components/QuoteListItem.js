import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteQuote } from '../actions/quotes';

const QuoteListItem = ({ dispatch, ID, content, link, title }) => (
	<div className="quote-container">
		<article>{content}</article>
		<div className="quote-link-container">
			<h2 className="quote-title">{title}</h2>
			<a href={link} target="_blank" rel="noopener noreferrer" className="quote-link">See more</a>
		</div>
		<div className="btn-container">
			<button
				onClick={() => {
					dispatch(deleteQuote({ ID }))
				}}
				className="remove-quote"
				>Remove
			</button>
			<Link
				to={`/edit/${ID}`}
				className="edit-quote"
				>
				Edit
			</Link>
		</div>
	</div>
)

export default connect()(QuoteListItem);