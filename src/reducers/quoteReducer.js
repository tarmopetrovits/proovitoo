const quotesReducerDefaultState = '';

export default (state = quotesReducerDefaultState, action) => {
	switch (action.type) {
		case 'ADD_QUOTE':
			return [
				...state,
				action.quote
			]
		case 'DELETE_QUOTE':
			return state.filter(({ ID }) => ID !== action.ID);
		case 'EDIT_QUOTE':
			return state.map((quote) => {
				if (quote.ID === action.ID) {
					return {
						...quote,
						...action.updates
					}
				} else {
					return quote;
				}
			})
		default:
			return state;
	}
}